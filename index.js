console.log('hola');

var unsplash = new Vue({
    el: "#app-unsplash",
    data: {
        images: [],
        searchInput: '',
        pageCount: 1,
        searchPage: 1,
        searched: false
    },
    methods: {
        getImages() {
            $.ajax({
                url: 'https://api.unsplash.com/photos?page=' + this.pageCount +
                    '&per_page=10&client_id=0ed1253bca8dcabbd6e40e6b7a5dee34e5f611e8732fd78dcf162ab7679e687c',
                dataType: 'json',
                success: function () {

                },
                beforeSend: function () {

                },
                complete: function (data) {
                    for (let i = 0; i < data.responseJSON.length; i++) {
                        unsplash.images.push(data.responseJSON[i])
                    }
                    unsplash.pageCount++;
                }
            });
        },
        getSearch(searchString) {
            $.ajax({
                url: 'https://api.unsplash.com/search/photos?page=' + this.searchPage +
                    '&query=' + searchString +
                    '&per_page=10&client_id=0ed1253bca8dcabbd6e40e6b7a5dee34e5f611e8732fd78dcf162ab7679e687c',
                dataType: 'json',
                success: function () {

                },
                beforeSend: function () {

                },
                complete: function (data) {
                    if (unsplash.searched) {
                        for (let i = 0; i < data.responseJSON.results.length; i++) {
                            unsplash.images.push(data.responseJSON.results[i])
                        }
                        unsplash.searchPage++;
                    } else {
                        unsplash.searched = true;
                        unsplash.images.splice(0, unsplash.images.length);
                        for (let i = 0; i < data.responseJSON.results.length; i++) {
                            unsplash.images.push(data.responseJSON.results[i])
                        }
                        unsplash.searchPage++;
                    }
                }
            });
        },
        scroll() {
            window.onscroll = () => {
                let bottomOfWindow = document.documentElement.scrollTop + window.innerHeight ===
                    document.documentElement.offsetHeight;

                if (bottomOfWindow) {
                    if (unsplash.searched) {
                        this.getSearch(unsplash.searchInput);
                    } else {
                        this.getImages();
                    }
                }
            };
        },

    },
    created() {
        this.getImages();
    },
    mounted() {
        this.scroll();
    }
});