# Front-End Test Case

This is a simple front-end code challenge created to serve two purposes:

1. Show off your skills,
1. Give us a better understanding of your skills.

## Additional Instructions

> * **Fork this repository**
> * After you're done, provide us the link to your repository.
> * Leave comments where you were not sure how to properly proceed.
> * Implementations **without** a README will be automatically rejected.
> * No pull requests
> * ES6 and above

## The Task

The programming challenge exists in 2 parts:
* **Part A**: HTML + CSS
* **Part B**: JavaScript

### PART A: HTML + CSS

I am using bootstrap framework because its a powerfull css framework and also being used by HalalNode
Part a contained in html file ChallengeA.html in this repository

### PART B: JavaScript
This repository deployed and live via https://halalnodechallenge.netlify.com/

Vue is also used in Halalnode, although personally i prefer Vue CLI instead of the CDN, the CLI version hosted on my github.
https://github.com/HSBL/vueuip
